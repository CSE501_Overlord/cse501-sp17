/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 * ©2016, Pavel Panchekha and the University of Washington.
 * Released under the MIT license.
 *
 * Rewritten in TypeScript and extended
 * ⓒ2017, Alex Polozov and the University of Washington.
 */
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
//// The AST
// This class represents all AST Nodes
var ASTNode = (function () {
    function ASTNode(type) {
        this.type = type;
    }
    ASTNode.prototype.execute = function (data) {
        throw new Error("Unimplemented AST node " + this.type);
    };
    ASTNode.prototype.optimize = function () {
        return this;
    };
    ASTNode.prototype.run = function (data) {
        return this.optimize().execute(data);
    };
    return ASTNode;
}());
// The ALL node just outputs all records.
var AllNode = (function (_super) {
    __extends(AllNode, _super);
    function AllNode() {
        return _super.call(this, "All") || this;
    }
    return AllNode;
}(ASTNode));
// The Filter node uses a callback to throw out some records
var FilterNode = (function (_super) {
    __extends(FilterNode, _super);
    function FilterNode(predicate) {
        var _this = _super.call(this, "Filter") || this;
        _this.predicate = predicate;
        return _this;
    }
    return FilterNode;
}(ASTNode));
// The Then node chains multiple actions on one data structure
var ThenNode = (function (_super) {
    __extends(ThenNode, _super);
    function ThenNode(first, second) {
        var _this = _super.call(this, "Then") || this;
        _this.first = first;
        _this.second = second;
        return _this;
    }
    //// 1.1 implement execute
    ThenNode.prototype.optimize = function () {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    };
    return ThenNode;
}(ASTNode));
//// 1.2 Write a Query
// Define the `theftsQuery` and `autoTheftsQuery` variables
var theftsQuery = new ASTNode("...");
var autoTheftsQuery = new ASTNode("...");
//// 1.3 Add Apply and Count Nodes
// ...
//// 1.4 Clean the data
var cleanupQuery = new ASTNode("...");
var Q = new AllNode();
//// 1.6 Reimplement queries with call-chaining
var cleanupQuery2 = Q; // ...
var theftsQuery2 = Q; // ...
var autoTheftsQuery2 = Q; // ...
//// 2.1 Optimize Queries
function AddOptimization(nodeType, opt) {
    var old = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function () {
        var newThis = old.call(this);
        return opt.call(newThis) || newThis;
    };
}
// ...
//// 2.2 Internal node types and CountIf
// ...
//// 3.1 Cartesian Products
// ...
//// 3.2 Joins
// ...
//// 3.3 Optimizing joins
// ...
//// 3.4 Join on fields
// ...
//// 3.5 Implement hash joins
// ...
//// 3.6 Optimize joins on fields to hash joins
// ...
//# sourceMappingURL=q.js.map
const antlr4 = require('antlr4');
const RegexListener = require('./RegexListener').RegexListener;

function RegexExplainer() {
    RegexListener.call(this);
    this._idGenerator = 0;
    this._captureNumber = 1;
    this.visibleStack = [];
    this.data = [];
    return this;
}

RegexExplainer.prototype = Object.create(RegexListener.prototype);
RegexExplainer.prototype.constructor = RegexExplainer;
exports.RegexExplainer = RegexExplainer;

RegexExplainer.prototype._generateId = function () {
    return "n" + this._idGenerator++;
};

RegexExplainer.prototype.enterEveryRule = function (ctx) {
    ctx._id = this._generateId();
};

RegexExplainer.prototype.exitEveryRule = function (ctx) {
    const lastVisible = this.visibleStack.slice(-1)[0];
    if (lastVisible && lastVisible === ctx) {
        this.visibleStack.pop();
    }
};

RegexExplainer.prototype.appendNode = function (ctx, text, icon) {
    const lastVisible = this.visibleStack.slice(-1)[0];
    const parent = lastVisible ? lastVisible._id : "#";
    this.data.push({
        "id": ctx._id,
        "parent": parent,
        "text": text,
        "state": {"opened": true},
        "icon": `glyphicon glyphicon-${icon || 'asterisk'}`
    });
    this.visibleStack.push(ctx);
};

RegexExplainer.prototype.enterRegex = function (ctx) {
    if (ctx.args.length <= 1) return;
    this.appendNode(ctx, "Match any of the following alternatives, in order:");
};

RegexExplainer.prototype.enterConcat = function (ctx) {
    this.appendNode(ctx, "Match this regex:");
};

RegexExplainer.prototype.enterStar = function (ctx) {
    this.appendNode(ctx, "Match the regex below between 0 and unlimited times (greedily):");
};

RegexExplainer.prototype.enterPlus = function (ctx) {
    this.appendNode(ctx, "Match the regex below between 1 and unlimited times (greedily):", "plus");
};

RegexExplainer.prototype.enterOptional = function (ctx) {
    this.appendNode(ctx, "Match the regex below between 0 and 1 times (greedily):", "question-sign");
};

RegexExplainer.prototype.enterAnyRegex = function (ctx) {
    this.appendNode(ctx, "Any character");
};

RegexExplainer.prototype.enterStartRegex = function (ctx) {
    this.appendNode(ctx, "Start of the line", "chevron-up");
};

RegexExplainer.prototype.enterEndRegex = function (ctx) {
    this.appendNode(ctx, "End of the line", "usd");
};

RegexExplainer.prototype.enterStringRegex = function (ctx) {
    this.appendNode(ctx, `String "${ctx.getText()}", literally`, "comment");
};

RegexExplainer.prototype.enterGroup = function (ctx) {
    const text = ctx.nocapture
        ? "Match the regex below as a non-capturing group:"
        : "Match the regex below and capture its match into backreference number " + this._captureNumber++;
    this.appendNode(ctx, text);
};

RegexExplainer.prototype.exitSet = function (ctx) {
    const members = ctx.members.map(x => x.representation).join("; ");
    const text = ctx.invert
        ? "Match anything that is not of the following: " + members
        : "Match any of the following: " + members;
    this.appendNode(ctx, text, "search");
};

RegexExplainer.prototype.enterCharSetElement = function (ctx) {
    ctx.representation = `'${ctx.getText()}'`;
};

RegexExplainer.prototype.enterRangeSetElement = function (ctx) {
    ctx.representation = `any between '${ctx.l.text}' and '${ctx.r.text}'`;
};
